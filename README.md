# Code snippets

This is a simple React app to present code snippets stored in a local database.

## Getting started

First, you'll need the latest LTS of [NodeJS](https://nodejs.org/en/), which is bundled with NPM. Once installed, you should be able to run `npm install` to install all dependencies.

We use [json-server](https://github.com/typicode/json-server) to provide a basic REST API over the "database" stored in `db/db.json`. To start the API server, run `npm run start-db`. The server code is located in `server.js`.

To start app, run `npm run start`.

You should now have the app running in your browser!

## Tasks

Currently, the snippets are displayed simply by their order in the database. Users would like to see the snippets ordered by votes, with the highest voted snippet first.

Also, you'll notice that the "Upvote" button doesn't do anything yet! This button should increment the vote counter by 1 each time it's clicked.

## Rules

What rules!

Feel free to modify any of the code as you see fit. How you'd like to tackle the problem is totally up to you. The code snippets are stored as base64'd strings in the database if you'd like to add more.

Good luck!
