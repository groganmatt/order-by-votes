import SnippetsContainer from "./containers/Snippets.container";

function App() {
  return <SnippetsContainer />;
}

export default App;
